package org.jrayme.dao;

import org.jrayme.bean.Producto;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IProductoDao extends JpaRepository<Producto, Integer> {

}
