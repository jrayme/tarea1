package org.jrayme.dao;

import org.jrayme.bean.Persona;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IPersonaDao extends JpaRepository<Persona, Integer> {

}
