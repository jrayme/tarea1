package org.jrayme.service;

import java.util.List;

import org.jrayme.bean.Persona;

public interface IPersonaService {

	Persona add(Persona p) throws Exception;

	Persona modify(Persona p) throws Exception;

	void delete(Integer id) throws Exception;

	List<Persona> findAll() throws Exception;

	Persona findById(Integer id) throws Exception;

}
