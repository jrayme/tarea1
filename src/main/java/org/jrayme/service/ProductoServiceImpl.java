package org.jrayme.service;

import java.util.List;

import org.jrayme.bean.Producto;
import org.jrayme.dao.IProductoDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductoServiceImpl implements IProductoService {

	@Autowired
	private IProductoDao dao;

	@Override
	public Producto add(Producto p) throws Exception {
		return dao.save(p);
	}

	@Override
	public Producto modify(Producto p) throws Exception {
		return dao.save(p);
	}

	@Override
	public void delete(Integer id) throws Exception {
		dao.deleteById(id);
	}

	@Override
	public List<Producto> findAll() throws Exception {
		return dao.findAll();
	}

	@Override
	public Producto findById(Integer id) throws Exception {
		return dao.findById(id).orElse(new Producto());
	}

}
