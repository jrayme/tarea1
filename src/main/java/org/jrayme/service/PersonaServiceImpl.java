package org.jrayme.service;

import java.util.List;

import org.jrayme.bean.Persona;
import org.jrayme.dao.IPersonaDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PersonaServiceImpl implements IPersonaService {

	@Autowired
	private IPersonaDao dao;

	@Override
	public Persona add(Persona p) throws Exception {
		return dao.save(p);
	}

	@Override
	public Persona modify(Persona p) throws Exception {
		return dao.save(p);
	}

	@Override
	public void delete(Integer id) throws Exception {
		dao.deleteById(id);
	}

	@Override
	public List<Persona> findAll() throws Exception {
		return dao.findAll();
	}

	@Override
	public Persona findById(Integer id) throws Exception {
		return dao.findById(id).orElse(new Persona());
	}

}
