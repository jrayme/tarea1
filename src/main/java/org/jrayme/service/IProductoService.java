package org.jrayme.service;

import java.util.List;

import org.jrayme.bean.Producto;

public interface IProductoService {

	Producto add(Producto p) throws Exception;

	Producto modify(Producto p) throws Exception;

	void delete(Integer id) throws Exception;

	List<Producto> findAll() throws Exception;

	Producto findById(Integer id) throws Exception;

}
