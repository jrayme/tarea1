package org.jrayme.bean;

import java.time.LocalDateTime;

import lombok.Data;

@Data
public class ExceptionResponse {

	private LocalDateTime fecha;

	private String mnsj;

	private String detalle;

}
