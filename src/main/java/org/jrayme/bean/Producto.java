package org.jrayme.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import lombok.Data;

@Data
@Entity
@Table(name = "tbl_producto")
public class Producto {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idProducto;

	@Size(min = 5, max = 50, message = "{nombre.size}")
	@Column(name = "nombre", nullable = false, length = 50)
	private String nombre;

	@Size(min = 5, max = 20, message = "{marca.size}")
	@Column(name = "marca", nullable = false, length = 20)
	private String marca;

}
