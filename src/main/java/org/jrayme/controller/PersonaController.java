package org.jrayme.controller;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.jrayme.bean.Persona;
import org.jrayme.exception.NotFoundException;
import org.jrayme.service.IPersonaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
@RequestMapping("/personas")
public class PersonaController {

	@Autowired
	private IPersonaService service;

	@GetMapping
	public ResponseEntity<List<Persona>> list() throws Exception {
		List<Persona> list = service.findAll();
		return new ResponseEntity<List<Persona>>(list, HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Persona> find(@PathVariable("id") Integer id) throws Exception {
		Persona obj = service.findById(id);

		if (obj.getIdPersona() == null) {
			throw new NotFoundException("No se encontro registro con ese id");
		}

		return new ResponseEntity<Persona>(obj, HttpStatus.OK);
	}

	@PostMapping
	public ResponseEntity<Persona> add(@Valid @RequestBody Persona p) throws Exception {
		Persona obj = service.add(p);

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(obj.getIdPersona()).toUri();
		return ResponseEntity.created(location).build();
	}

//	@PostMapping
//	public ResponseEntity<Persona> add(@Valid @RequestBody Persona p) throws Exception{
//		Persona obj = service.add(p);
//
//		return new ResponseEntity<Persona>(obj, HttpStatus.CREATED);
//	}

	@PutMapping
	public ResponseEntity<Persona> update(@Valid @RequestBody Persona p) throws Exception {
		Persona obj = service.modify(p);
		return new ResponseEntity<Persona>(obj, HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Void> delete(@PathVariable("id") Integer id) throws Exception {
		service.delete(id);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}

}
