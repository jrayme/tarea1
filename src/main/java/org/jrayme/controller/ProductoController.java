package org.jrayme.controller;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.jrayme.bean.Producto;
import org.jrayme.exception.NotFoundException;
import org.jrayme.service.IProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
@RequestMapping("/productos")
public class ProductoController {

	@Autowired
	private IProductoService service;

	@GetMapping
	public ResponseEntity<List<Producto>> list() throws Exception {
		List<Producto> list = service.findAll();
		return new ResponseEntity<List<Producto>>(list, HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Producto> find(@PathVariable("id") Integer id) throws Exception {
		Producto obj = service.findById(id);

		if (obj.getIdProducto() == null) {
			throw new NotFoundException("No se encontro registro con ese id");
		}

		return new ResponseEntity<Producto>(obj, HttpStatus.OK);
	}

	@PostMapping
	public ResponseEntity<Producto> add(@Valid @RequestBody Producto p) throws Exception {
		Producto obj = service.add(p);

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(obj.getIdProducto()).toUri();
		return ResponseEntity.created(location).build();
	}

//	@PostMapping
//	public ResponseEntity<Producto> add(@Valid @RequestBody Producto p) throws Exception {
//		Producto obj = service.add(p);
//
//		return new ResponseEntity<Producto>(obj, HttpStatus.CREATED);
//	}

	@PutMapping
	public ResponseEntity<Producto> update(@Valid @RequestBody Producto p) throws Exception {
		Producto obj = service.modify(p);
		return new ResponseEntity<Producto>(obj, HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Void> delete(@PathVariable("id") Integer id) throws Exception {
		service.delete(id);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}

}
