package org.jrayme.controller;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.LocaleResolver;

@RestController
@RequestMapping("/utils")
public class UtilController {

	@Autowired
	private LocaleResolver localeResolver;

	@Autowired
	private HttpServletRequest request;

	@Autowired
	private HttpServletResponse response;

	@GetMapping("/locale/{loc}")
	public ResponseEntity<Void> changeLocale(@PathVariable("loc") String loc) {

		Locale locale = null;

		switch (loc) {
		case "en":
			locale = Locale.ENGLISH;
			break;
		default:
			locale = Locale.ROOT;
			break;
		}

		localeResolver.setLocale(request, response, locale);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}

}
