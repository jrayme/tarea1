package org.jrayme.exception;

import java.time.LocalDateTime;
import java.util.stream.Collectors;

import org.jrayme.bean.ExceptionResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@RestController
public class ResponseExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(Exception.class)
	public final ResponseEntity<ExceptionResponse> manejaTodasLasExcepciones(Exception ex, WebRequest request) {
		ExceptionResponse response = new ExceptionResponse();
		response.setFecha(LocalDateTime.now());
		response.setMnsj(ex.getMessage());
		response.setDetalle(request.getDescription(false));

		return new ResponseEntity<ExceptionResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(NotFoundException.class)
	public ResponseEntity<ExceptionResponse> manejarModeloNotFoundException(NotFoundException ex, WebRequest request) {

		ExceptionResponse response = new ExceptionResponse();
		response.setFecha(LocalDateTime.now());
		response.setMnsj(ex.getMessage());
		response.setDetalle(request.getDescription(false));

		return new ResponseEntity<ExceptionResponse>(response, HttpStatus.NOT_FOUND);
	}

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {

		String mnsj = ex.getBindingResult().getAllErrors().stream().map(x -> {
			return x.getDefaultMessage().toString().concat(", ");
		}).collect(Collectors.joining());

		ExceptionResponse response = new ExceptionResponse();
		response.setFecha(LocalDateTime.now());
		response.setMnsj(mnsj);
		response.setDetalle(request.getDescription(false));

		return new ResponseEntity<Object>(response, HttpStatus.BAD_REQUEST);
	}

}
